import copy

class PassTimeCommand:
    def __init__(self, time_to_add):
        self.time_to_add = time_to_add

class SetResouceCommand:
    def __init__(self, time, amount):
        self.amount = amount
        self.time = time

class PersistStateCommand:
    pass
class CrashAndRestoreCommand:
    pass
class SendToQueueCommand:
    pass

class OutMessageQueue:
    def __init__(self):
        self.queue = []

    def send(self, lsn, current_time, time, from_value, to_value, comment):
        self.queue.append({"lsn": lsn,
                           "current_time": current_time,
                           "timestamp": "{}-{}".format(time, time+PSEUDO_QUANTUM),
                           "from_amount": from_value["amount"],
                           "from_duration": from_value["duration"],
                           "to_amount": to_value["amount"],
                           "to_duration": to_value["duration"],
                           "comment": comment})

    def print_queue(self):
        for q in self.queue:
            print(q)

out_queue = OutMessageQueue()

class InMessageQueue:
    def __init__(self):
        self.queue = []

    def send(self, time, amount):
        self.queue.append({"time": time, "amount": amount, "offset": len(self.queue), "read": 0})

    def get(self, offset):
        self.queue[offset]["read"] += 1
        return self.queue[offset]

    def print_queue(self):
        for q in self.queue:
            print(q)

    def get_last_offset(self):
        return len(self.queue)-1


in_message = InMessageQueue()

PSEUDO_QUANTUM = 10

class ResourceTracker:
    def __init__(self):
        self.current_time = 0
        self.last_send_time = 0
        self.states_history = [{"time": 0, "amount": 0, "offset": -1}]
        self.offset = 0
        self.resend_commands = []

        self.persist_state()


    def try_read_message(self):
        if in_message.get_last_offset() < self.offset:
            return False

        m = in_message.get(self.offset)
        self.offset+=1

        if self.states_history[-1]["time"]<m["time"]:
            # событие пришло вовремя
            self.states_history.append(m)
        else:
            new_history = []
            m_added = False
            for s in self.states_history:
                if s["time"] < m["time"]:
                    new_history.append(s)
                else:
                    if not m_added:
                        new_history.append(m)
                        m_added = True
                    new_history.append(s)
            self.states_history = new_history

        if m["time"] < self.last_send_time:
            #необходимо перепослать события
            # устаревшее событие
            # нужно переотправить все интервалы, на которые повлияло событие
            from_time = (m["time"] // PSEUDO_QUANTUM) * PSEUDO_QUANTUM
            print("resended from {} to {} because of {}".format(from_time, self.last_send_time, m))
            self.resend_commands.append({"from_time": from_time, "to_time": self.last_send_time, "excluded_offsets": [m["offset"]], "reason": [m]})

        return True

    def persist_state(self):
        self.db_states_history = copy.deepcopy(self.states_history)
        self.db_last_send_time = self.last_send_time
        self.db_offset = self.offset
        self.db_resend_commands = copy.deepcopy(self.resend_commands)


    def crash_and_restore(self):
        self.states_history = copy.deepcopy(self.db_states_history)
        self.last_send_time = self.db_last_send_time
        self.offset = self.db_offset

        self.resend_commands = copy.deepcopy(self.db_resend_commands)

        while self.try_read_message():
            pass

        self.send_to_queue_after_restore_service()

    def send_to_queue(self, from_time, to_time, excluded_offsets, comment = ""):
        if from_time + PSEUDO_QUANTUM > self.current_time:
            return from_time # еще не пора отсылать

        while from_time + PSEUDO_QUANTUM <= to_time:
            from_value = {"amount":0, "duration":0}
            if len(excluded_offsets) > 0:
                from_value = self.__find_amount_and_duration_for_time__(from_time, excluded_offsets)

            to_value = self.__find_amount_and_duration_for_time__(from_time, [])

            if from_value["amount"]!=to_value["amount"] or from_value["duration"] != to_value["duration"]:
                out_queue.send("{}-{}-{}".format(self.last_send_time, self.offset, from_time),self.current_time, from_time, from_value, to_value, comment)

            if self.last_send_time < from_time:
                self.last_send_time = from_time

            from_time += PSEUDO_QUANTUM

        return from_time


    def send_to_queue_after_restore_service(self):
        for resend_command in self.resend_commands:
            self.send_to_queue(resend_command["from_time"], resend_command["to_time"], resend_command["excluded_offsets"],
                               "restoring >>>>> resended from {} to {} because of {}".format(resend_command["from_time"], resend_command["to_time"], resend_command["reason"]))
        self.resend_commands = []

        self.last_send_time = self.send_to_queue(self.last_send_time, self.current_time, [], "restoring >>>>> ")


    def send_to_queue_from_last_send_time(self):
        for resend_command in self.resend_commands:
            self.send_to_queue(resend_command["from_time"], resend_command["to_time"], resend_command["excluded_offsets"],
                               "resended from {} to {} because of {}".format(resend_command["from_time"], resend_command["to_time"], resend_command["reason"]))
        self.resend_commands = []

        self.last_send_time = self.send_to_queue(self.last_send_time, self.last_send_time + PSEUDO_QUANTUM, [])


    def __find_amount_and_duration_for_time__(self, time, exclused_offsets):
        sh = [el for el in self.states_history if el["offset"] not in exclused_offsets]

        if sh[-1]["time"]<=time:
            return {"amount": sh[-1]["amount"], "duration": PSEUDO_QUANTUM}
        else:
            from_i = 0
            for i in range(len(sh)):
                if sh[i]["time"]<time and sh[i+1]["time"]>time or sh[i]["time"] == time:
                    from_i = i
                    break

            amount = sh[from_i]["amount"]
            for i in range(len(sh)):
                if i > from_i:
                    if sh[i]["time"]<time+PSEUDO_QUANTUM:
                        amount = max(amount, sh[i]["amount"])
            return {"amount": amount, "duration": PSEUDO_QUANTUM} #todo учесть удаление ресурса в duration

    def print_resource_history(self):
        for h in self.states_history:
            print(h)


resource_tracker = ResourceTracker()


class CommandQueue:
    def __init__(self):
        self.queue = []

    def Add(self, command):
        self.queue.append(command)
        return self

    def Track(self):
        for c in self.queue:
            if type(c) is PassTimeCommand:
                resource_tracker.current_time += c.time_to_add
            if type(c) is SetResouceCommand:
                in_message.send(c.time, c.amount)
                resource_tracker.try_read_message()
            if type(c) is PersistStateCommand:
                resource_tracker.persist_state()
            if type(c) is CrashAndRestoreCommand:
                resource_tracker.crash_and_restore()
            if type(c) is SendToQueueCommand:
                resource_tracker.send_to_queue_from_last_send_time()


def track_with_crash():
    commands = CommandQueue()
    commands.Add(PassTimeCommand(1))
    commands.Add(SetResouceCommand(2, 1))
    commands.Add(PassTimeCommand(1))
    commands.Add(SetResouceCommand(4, 3))
    commands.Add(PassTimeCommand(10))
    commands.Add(SendToQueueCommand())
    commands.Add(SetResouceCommand(18,2))

    commands.Add(PersistStateCommand())

    commands.Add(PassTimeCommand(10))
    commands.Add(SetResouceCommand(3,4))
    commands.Add(PassTimeCommand(10))
    commands.Add(SendToQueueCommand())
    commands.Add(SendToQueueCommand())
    commands.Add(PassTimeCommand(10))
    commands.Add(SendToQueueCommand())
    commands.Add(PassTimeCommand(10))

    commands.Add(CrashAndRestoreCommand())

    commands.Add(SetResouceCommand(25, 7))
    commands.Add(SendToQueueCommand())
    commands.Add(PassTimeCommand(1))
    #commands.Add(CrashAndRestoreCommand())
    commands.Add(SetResouceCommand(1, 9))
    commands.Add(SendToQueueCommand())

    commands.Track()
    in_message.print_queue()
    out_queue.print_queue()
    resource_tracker.print_resource_history()


def track_with_delete():
    commands = CommandQueue()
    commands.Add(PassTimeCommand(1))
    commands.Add(SetResouceCommand(2, 1))
    commands.Add(PassTimeCommand(1))
    commands.Add(SetResouceCommand(4, 3))
    commands.Add(PassTimeCommand(10))
    commands.Add(SendToQueueCommand())

    commands.Add(SetResouceCommand(18, 0))
    commands.Add(PassTimeCommand(10))
    commands.Add(SendToQueueCommand())
    commands.Add(PassTimeCommand(10))
    commands.Add(SendToQueueCommand())
    commands.Add(PassTimeCommand(10))
    commands.Add(SendToQueueCommand())

    commands.Track()
    in_message.print_queue()
    out_queue.print_queue()
    resource_tracker.print_resource_history()


#track_with_delete()
track_with_crash()